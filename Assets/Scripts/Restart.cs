﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Restart : MonoBehaviour
{

    //public static List<EntranceScript>;

    public string SceneName;
    public bool EntranceEnabled = true;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (EntranceEnabled && other.CompareTag("Player"))
        {
            SceneManager.LoadScene(SceneName);
        }
    }
    // Use this for initialization

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
